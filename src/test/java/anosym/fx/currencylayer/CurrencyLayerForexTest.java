package anosym.fx.currencylayer;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import anosym.common.Currency;
import anosym.fx.Quote;
import lombok.SneakyThrows;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 */
public class CurrencyLayerForexTest {

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Test
  @SneakyThrows
  public void verifyQuotes() {
    final CurrencyLayerForex forex
            = objectMapper.readValue(getClass().getResourceAsStream("/fx.json"), CurrencyLayerForex.class);
    assertThat(forex.getQuotes(), hasItem(new Quote(Currency.JPY, new BigDecimal("110.60797"))));
  }

}
