package anosym.fx;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

import anosym.common.Currency;
import lombok.Value;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:49:44 PM
 */
@Value
public class Quote {

    @Nonnull
    private final Currency quote;

    @Nonnull
    private final BigDecimal rate;

}
