package anosym.fx;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import javax.annotation.Nonnull;

import anosym.common.Currency;
import anosym.fx.Fx.FxImpl;
import anosym.fx.config.ForexConfigurationService;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 11:32:52 PM
 */
@Slf4j
@ApplicationScoped
public class ForexRateService {

  @Any
  @Inject
  private Instance<ForexRateProvider<? extends Forex>> forexRateProviders;

  @Inject
  private ForexConfigurationService forexConfigurationService;

  private Forex forex;

  @PostConstruct
  void initForex() {
    final ForexConfigurationService.ForexProvider forexProvider = forexConfigurationService.getForexProvider();
    final Fx feign = new FxImpl(forexProvider.getCode());
    final ForexRateProvider<?> forexRateProvider = forexRateProviders.select(feign).get();
    this.forex = forexRateProvider.getFxRates();

    log.info("Downloaded forex: {}", forex);
  }

  @Nonnull
  public Optional<BigDecimal> getExchangeRate(@NonNull final Currency base, @NonNull final Currency quote) {
    final Optional<BigDecimal> defaultBaseRate = findExchangeRate(base, quote);
    if (defaultBaseRate.isPresent()) {
      return defaultBaseRate;
    }

    //Invert the inverse rate.
    return findExchangeRate(quote, base)
            .map((inverseRate) -> BigDecimal.ONE.divide(inverseRate, RoundingMode.UP));
  }

  @Nonnull
  private Optional<BigDecimal> findExchangeRate(@NonNull final Currency base, @NonNull final Currency quote) {
    if (base == quote) {
      return Optional.of(BigDecimal.ONE);
    }

    final Quote quotedRate = forex.getQuotes()
            .stream()
            .filter((q) -> q.getQuote() == quote)
            .findFirst()
            .orElse(null);
    if (quotedRate == null) {
      return Optional.empty();
    }

    if (forex.getSource() == base) {
      return Optional.of(quotedRate.getRate());
    }

    //Convert to source currency, and then to base
    final Quote baseRate = forex.getQuotes()
            .stream()
            .filter((q) -> q.getQuote() == base)
            .findFirst()
            .orElse(null);
    if (baseRate == null) {
      return Optional.empty();
    }

    final BigDecimal rate = quotedRate.getRate().divide(baseRate.getRate(), 5, RoundingMode.UP);
    return Optional.of(rate);
  }

}
