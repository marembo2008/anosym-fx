package anosym.fx;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import anosym.common.Currency;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 3, 2015, 12:04:23 AM
 */
public interface Forex {

    @Nonnull
    Currency getSource();

    @Nonnull
    List<Quote> getQuotes();

    @Nullable
    String getTerms();

    @Nullable
    String getPrivacy();

}
