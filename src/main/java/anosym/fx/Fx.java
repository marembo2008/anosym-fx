package anosym.fx;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Qualifier;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 12, 2018, 10:26:34 PM
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD, METHOD, TYPE, PARAMETER})
public @interface Fx {

  String value();

  @AllArgsConstructor
  @SuppressWarnings("AnnotationAsSuperInterface")
  class FxImpl extends AnnotationLiteral<Fx> implements Fx {

    @NonNull
    private final String value;

    @Override
    public String value() {
      return value;
    }

  }

}
