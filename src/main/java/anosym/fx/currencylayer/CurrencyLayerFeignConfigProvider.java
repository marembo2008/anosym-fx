package anosym.fx.currencylayer;

import anosym.feign.config.FeignConfig;
import anosym.feign.config.FeignConfigProvider;
import anosym.fx.config.CurrencyLayerConfigurationService;
import jakarta.enterprise.inject.spi.CDI;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:30:45 PM
 */
@Slf4j
@FeignConfig("currencylayer")
public class CurrencyLayerFeignConfigProvider implements FeignConfigProvider {

  @Override
  public String getEndPoint(@NonNull final String feignClientInstanceId) {
    log.debug("Getting api-endopint for: {}", feignClientInstanceId);

    return CDI.current().select(CurrencyLayerConfigurationService.class).get().getApiUrl();
  }

}
