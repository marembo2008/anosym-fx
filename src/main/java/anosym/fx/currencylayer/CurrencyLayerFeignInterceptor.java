package anosym.fx.currencylayer;

import anosym.feign.FeignClientId;
import anosym.feign.interceptor.ApiRequestInterceptor;
import anosym.feign.interceptor.FeignInterceptor;
import anosym.fx.config.CurrencyLayerConfigurationService;
import anosym.fx.config.ForexConfigurationService;
import feign.RequestTemplate;
import jakarta.enterprise.inject.spi.CDI;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:32:59 PM
 */
@Slf4j
@FeignInterceptor("currencylayer")
public class CurrencyLayerFeignInterceptor implements ApiRequestInterceptor {

  @Override
  public void apply(@NonNull final FeignClientId feignClientId, @NonNull final RequestTemplate requestTemplate) {
    log.debug("Intercepting api request for: {}", feignClientId);

    final CurrencyLayerConfigurationService currencyLayerConfigurationService
            = CDI.current().select(CurrencyLayerConfigurationService.class).get();
    final ForexConfigurationService forexConfigurationService
            = CDI.current().select(ForexConfigurationService.class).get();
    requestTemplate.query("access_key", currencyLayerConfigurationService.getAccessKey())
            .query("source", forexConfigurationService.getBaseCurrency().name());
  }

}
