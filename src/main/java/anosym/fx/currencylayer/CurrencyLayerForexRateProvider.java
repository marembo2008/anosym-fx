package anosym.fx.currencylayer;

import javax.annotation.Nonnull;

import anosym.fx.ForexRateProvider;
import anosym.fx.Fx;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:19:43 PM
 */
@ApplicationScoped
@Fx("currencylayer")
public class CurrencyLayerForexRateProvider implements ForexRateProvider<CurrencyLayerForex> {

  @Inject
  private CurrencyLayerForexRateApi currencyLayerForexRateApi;

  @Nonnull
  @Override
  public CurrencyLayerForex getFxRates() {
    final CurrencyLayerForex forex = currencyLayerForexRateApi.getFxRates();
    if (!forex.isSuccess()) {
      throw new RuntimeException(String.valueOf(forex.getError()));
    }

    return forex;
  }

}
