package anosym.fx.currencylayer;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 28, 2019, 11:42:30 PM
 */
@Data
public class ForexError {

  private Integer code;

  private String description;

}
