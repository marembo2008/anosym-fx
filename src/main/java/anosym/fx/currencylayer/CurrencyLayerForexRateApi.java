package anosym.fx.currencylayer;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.RequestLine;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:19:43 PM
 */
@FeignClient("currencylayer")
public interface CurrencyLayerForexRateApi {

    @RequestLine("GET /live")
    @Headers("Accept: application/json")
    CurrencyLayerForex getFxRates();

}
