package anosym.fx.currencylayer;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import anosym.common.Currency;
import anosym.fx.Forex;
import anosym.fx.Quote;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:20:41 PM
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CurrencyLayerForex implements Forex {

  private boolean success;

  private long timestamp;

  @Nonnull
  private Currency source;

  @Nonnull
  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private Map<String, BigDecimal> quotes;

  @Nullable
  private String terms;

  @Nullable
  private String privacy;

  @Nullable
  private ForexError error;

  @Nonnull
  @Override
  public List<Quote> getQuotes() {
    return quotes.entrySet()
            .stream()
            .map((quote) -> {
              try {
                final String quoteCurrIsoCode = quote.getKey().substring(3);
                final Currency quoteCurr = Currency.fromIsoCode(quoteCurrIsoCode);
                return new Quote(quoteCurr, quote.getValue());
              } catch (final IllegalArgumentException ex) {
                log.info("No currency for quote: {}", quote);
                return null;
              }
            })
            .filter(Objects::nonNull)
            .collect(toList());
  }

}
