package anosym.fx.config;

import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static khameleon.core.ApplicationMode.LIVE;
import static khameleon.core.ApplicationMode.LOCAL;

import javax.annotation.Nonnull;

import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:25:54 PM
 */
@Config
@ConfigRoot
@AppContext(applicationModes = {DEVELOPMENT, LOCAL, LIVE})
public interface CurrencyLayerConfigurationService {

    @Nonnull
    @Info("Currency Layer access-key. Must be provided")
    String getAccessKey();

    @Nonnull
    @Default("http://apilayer.net")
    String getApiUrl();

}
