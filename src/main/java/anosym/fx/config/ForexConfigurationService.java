package anosym.fx.config;

import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static khameleon.core.ApplicationMode.LIVE;
import static khameleon.core.ApplicationMode.LOCAL;

import javax.annotation.Nonnull;

import anosym.common.Currency;
import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 11:26:56 PM
 */
@Config
@ConfigRoot
@AppContext(applicationModes = {DEVELOPMENT, LOCAL, LIVE})
public interface ForexConfigurationService {

    @Nonnull
    @Default("USD")
    Currency getBaseCurrency();

    @Nonnull
    @Default("CURRENCY_LAYER")
    ForexProvider getForexProvider();

    @AllArgsConstructor
    enum ForexProvider {

        CURRENCY_LAYER("currencylayer", "Currency Layer"),
        OPEN_EXCHANGE("openexchange", "Open Exchange Rates");

        @Getter
        private final String code;

        private final String description;

        @Override
        public String toString() {
            return description;
        }

    }

}
