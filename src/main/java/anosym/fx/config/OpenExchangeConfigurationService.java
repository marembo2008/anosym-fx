package anosym.fx.config;

import static khameleon.core.ApplicationMode.DEVELOPMENT;
import static khameleon.core.ApplicationMode.LIVE;
import static khameleon.core.ApplicationMode.LOCAL;

import javax.annotation.Nonnull;

import khameleon.core.annotations.AppContext;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 15, 2015, 8:00:44 PM
 */
@Config
@ConfigRoot
@AppContext(applicationModes = {DEVELOPMENT, LOCAL, LIVE})
public interface OpenExchangeConfigurationService {

    @Nonnull
    @Default("17039831aa5a4dcaa6b480b9485e62f0")
    String getAppId();

    @Nonnull
    @Default("https://openexchangerates.org")
    String getApiUrl();

}
