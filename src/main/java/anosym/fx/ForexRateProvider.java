package anosym.fx;

import javax.annotation.Nonnull;

public interface ForexRateProvider<T extends Forex> {

    @Nonnull
    T getFxRates();

}
