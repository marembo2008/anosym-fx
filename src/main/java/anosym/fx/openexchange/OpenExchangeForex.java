package anosym.fx.openexchange;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;

import anosym.common.Currency;
import anosym.fx.Forex;
import anosym.fx.Quote;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 3, 2015, 12:04:23 AM
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OpenExchangeForex implements Forex {

    @Nonnull
    @JsonProperty("base")
    private Currency source;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Map<String, BigDecimal> rates;

    @Nullable
    @JsonProperty("disclaimer")
    private String terms;

    @Nullable
    @JsonProperty("license")
    private String privacy;

    @Nonnull
    @Override
    public List<Quote> getQuotes() {
        return rates.entrySet()
                .stream()
                .map(this::toQuote)
                .filter(Objects::nonNull)
                .collect(toList());
    }

    @Nullable
    private Quote toQuote(@Nonnull final Entry<String, BigDecimal> quoteValue) {
        final Currency currency = fromIsoCodeOrNull(quoteValue.getKey());
        if (currency == null) {
            return null;
        }

        return new Quote(currency, quoteValue.getValue());
    }

    @Nullable
    private Currency fromIsoCodeOrNull(@Nonnull final String isoCode) {
        try {
            return Currency.fromIsoCode(isoCode);
        } catch (Exception e) {
            log.warn("No currency defined for isocode: {}", isoCode);
            // ignore
            return null;
        }
    }

}
