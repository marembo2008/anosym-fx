package anosym.fx.openexchange;

import anosym.feign.FeignClient;
import feign.Headers;
import feign.RequestLine;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 15, 2015, 7:59:09 PM
 */
@FeignClient("openexchange")
public interface OpenExchangeRateApi {

    @RequestLine("GET api/latest.json")
    @Headers("Accept: application/json")
    OpenExchangeForex getFxRates();

}
