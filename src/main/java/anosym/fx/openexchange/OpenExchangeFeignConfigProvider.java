package anosym.fx.openexchange;

import anosym.feign.config.FeignConfig;
import anosym.feign.config.FeignConfigProvider;
import anosym.fx.config.OpenExchangeConfigurationService;
import jakarta.enterprise.inject.spi.CDI;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:30:45 PM
 */
@Slf4j
@FeignConfig("openexchange")
public class OpenExchangeFeignConfigProvider implements FeignConfigProvider {

  @Override
  public String getEndPoint(@NonNull final String feignClientInstanceId) {
    log.debug("Getting api-endopint for: {}", feignClientInstanceId);

    return CDI.current().select(OpenExchangeConfigurationService.class).get().getApiUrl();
  }

}
