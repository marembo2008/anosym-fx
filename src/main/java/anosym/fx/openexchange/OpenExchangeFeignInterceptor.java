package anosym.fx.openexchange;

import anosym.feign.FeignClientId;
import anosym.feign.interceptor.ApiRequestInterceptor;
import anosym.feign.interceptor.FeignInterceptor;
import anosym.fx.config.ForexConfigurationService;
import anosym.fx.config.OpenExchangeConfigurationService;
import feign.RequestTemplate;
import jakarta.enterprise.inject.spi.CDI;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 11, 2018, 10:32:59 PM
 */
@Slf4j
@FeignInterceptor("openexchange")
public class OpenExchangeFeignInterceptor implements ApiRequestInterceptor {

  @Override
  public void apply(@NonNull final FeignClientId feignClientId, @NonNull final RequestTemplate requestTemplate) {
    log.debug("Intercepting api request for: {}", feignClientId);

    final OpenExchangeConfigurationService openExchangeConfigurationService
            = CDI.current().select(OpenExchangeConfigurationService.class).get();
    final ForexConfigurationService forexConfigurationService
            = CDI.current().select(ForexConfigurationService.class).get();
    requestTemplate.query("app_id", openExchangeConfigurationService.getAppId())
            .query("base", forexConfigurationService.getBaseCurrency().name());
  }

}
