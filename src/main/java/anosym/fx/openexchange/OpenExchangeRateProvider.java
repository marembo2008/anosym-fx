package anosym.fx.openexchange;

import anosym.fx.ForexRateProvider;
import anosym.fx.Fx;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 15, 2015, 7:59:09 PM
 */
@ApplicationScoped
@Fx("openexchange")
public class OpenExchangeRateProvider implements ForexRateProvider<OpenExchangeForex> {

  @Inject
  private OpenExchangeRateApi openExchangeRateApi;

  @Override
  public OpenExchangeForex getFxRates() {
    return openExchangeRateApi.getFxRates();
  }

}
